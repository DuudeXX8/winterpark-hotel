import "owl.carousel";
import 'owl.carousel/src/scss/owl.carousel.scss';
import "owl.carousel/src/scss/owl.theme.default.scss";
import "owl.carousel/src/scss/_theme.default.scss";


// let owl = $("#main-bg-slider");

// popup magnific


//magnific popup end

let [owl, cardsRow , offerCard, gallery, news,clientsPrev] = [
    $("#main-bg-slider"),
    $("#slider-card"),
    $("#offer-card"),
    $("#gallery"),
    $("#news-card"),
    $(".clients-prev")
];
owl.owlCarousel({
    slideSpeed : 300,
    paginationSpeed : 400,
    singleItem: true,
	pagination: false,
    rewindSpeed: 500,
    items:1, 
    dots:false,
    nav:true,   
    navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #fff;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>','<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #fff;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
});

let owlArray = [cardsRow, offerCard, gallery,news];


clientsPrev.owlCarousel({
    loop:true,
    margin:20,
    nav:true,
    items: 1,
    navigation:true,
    navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #fff;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>','<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #fff;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
});
owlArray.forEach((i) => {
    i.owlCarousel({
        loop: true,
        margin: 0,
        dots: true,
        nav: false,
        items: 3,
        responsive:{
            0:{
                items:1,
            },
            768:{
                items:2,
            },
            992: {
                items:3,
            }
            
        }
    });
});

