import { MagnificPopup } from 'magnific-popup';

import "magnific-popup/src/css/_settings.scss";
import "magnific-popup/src/css/main.scss";

$('.popup-gallery').magnificPopup({
    delegate: '.owl-item:not(.cloned) a',
    type: 'image',
    removalDelay: 500, //delay removal by X to allow out-animation
    callbacks: {
        beforeOpen: function() {
            // just a hack that adds mfp-anim class to markup 
             this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
             this.st.mainClass = this.st.el.attr('data-effect');
        }
    },
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
    },
    callbacks: {
        open: function() {
            $('html').css('margin-right', 0);
        }
    }

});
$('#inline-popups, #main-submit').magnificPopup({
    delegate: 'a',
    removalDelay: 500, //delay removal by X to allow out-animation
    callbacks: {
        beforeOpen: function () {
            this.st.mainClass = this.st.el.attr('data-effect');
        },
        open: function () {
            $('html').css('margin-right', 0);
        }
    },
    midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
});