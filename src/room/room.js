
/*==== Include Js files ======*/

import $ from "jquery";
import jQuery from "jquery";
import "../owlConfig";
import "../magnificPopup";

// import Calendar Plugin
global.moment = require('moment');
require('tempusdominus-bootstrap-4');
// ======== Include SCSS FILES ============

import "../../styles/index/header.scss";
import "../../styles/index/footer.scss";


import "../../styles/room/room.scss";
import "normalize.scss/normalize.scss";
import  "bootstrap/scss/bootstrap.scss";
import "bootstrap/dist/js/bootstrap.bundle.min.js";


$(document).ready(function(){
    ///hover container lang menu
    $("#lang-menu").on("click",function(){
            $(this).addClass("cls-border-lang");
            $(this).children().eq(0).addClass("cls-borderbottom-lang");
                $("#lang-menu ul").stop().slideToggle(100);
      },
      function(){
            $(this).removeClass("cls-border-lang");
            $(this).children().eq(0).removeClass("cls-borderbottom-lang");
            $("#lang-menu ul").stop().slideToggle(100);  
      }
    );
    /// click languages
    $("#lang-menu ul li").on("click", function(){
          //select lang and apply changes
          $lang = $(this).text();
          $("#lang-menu div").val($lang);
    });
    // Datetimepicker configuration
    $(function () {
        $('#datetimepicker1, #datetimepicker2 , #datetimepicker3 , #datetimepicker4,#datetimepicker5, #datetimepicker6').datetimepicker({
            locale: 'ru',
            format: 'L',
            vertical:'top',
            startDate: '-3d'
        });
    }); 
    
    $(document).mouseup(function (e) {
        var container = $("#lang-menu");
        var ulLi = $("#lang-menu  ul")
        if (container.has(e.target).length === 0){
            ulLi.hide();
        }
    });
    
    $('.burger, .overlay').click(function(){
        $('.burger').toggleClass('clicked');
        $('.overlay').toggleClass('show');
        $('nav').toggleClass('show');
        $('body').toggleClass('overflow');
    });
    // Bind to the resize event of the window object
    $(window).on("resize", function () {
        // Set .right's width to the window width minus 480 pixels
        let insertFollowUs = _ => {
            let w = window.innerWidth;
            let roomOverview = document.querySelector(".room-overview");
            let roomMainTxt = document.querySelector(".room-main-text");
            let overview = document.querySelector(".overview");
            let btnContainer = document.querySelector(".room-overview > .btn-container");
            if (w <= 568) {
                $(btnContainer).insertAfter(roomMainTxt);
                
            } else  {
                $(btnContainer).insertAfter(overview);
               
            }
        }
        insertFollowUs();
        // Invoke the resize event immediately
    }).resize();
});


// ===== CHECK WIDTH FUNCTION ======
function checkWidth(init)
{
    /* -- If browser resized, check width again  --*/
    if ($(window).width() <= 992) {
        $('#sm-desc').addClass('p-0');  
    }
    else {
        if ($(window).width() > 992) {
            if($("#sm-desc").hasClass("p-0")){
                $('.room-information > .container-fluid').removeClass('p-0'); 
            }
        }
    }
}


$(document).ready(function() {
    checkWidth();

    $(window).resize(function() {
        checkWidth();
    });
});
  
