/*==== Include Js files ======*/

import $ from "jquery";
import jQuery from "jquery";
import "./owlConfig";
import "./magnificPopup";

// import Calendar Plugin
global.moment = require('moment');
require('tempusdominus-bootstrap-4');

/*==== Include Scss files ======*/
import "../styles/index/header.scss";
import "../styles/index/footer.scss";


import "../styles/index.scss";
import "normalize.scss/normalize.scss";
import  "bootstrap/scss/bootstrap.scss";
import "bootstrap/dist/js/bootstrap.bundle.min.js";








$(document).ready(function(){
    ///hover container lang menu
    $("#lang-menu").on("click",function(){
            $(this).addClass("cls-border-lang");
            $(this).children().eq(0).addClass("cls-borderbottom-lang");
                $("#lang-menu ul").stop().slideToggle(100);
      },
      function(){
            $(this).removeClass("cls-border-lang");
            $(this).children().eq(0).removeClass("cls-borderbottom-lang");
            $("#lang-menu ul").stop().slideToggle(100);  
      }
    );
    /// click languages
    $("#lang-menu ul li").on("click", function(){
          //select lang and apply changes
          $lang = $(this).text();
          $("#lang-menu div").val($lang);
    });
    
    $(document).mouseup(function (e) {
        var container = $("#lang-menu");
        var ulLi = $("#lang-menu  ul")
        if (container.has(e.target).length === 0){
            ulLi.hide();
        }
    });
    // Datetimepicker configuration
    $(function () {
        $('#datetimepicker1, #datetimepicker2 , #datetimepicker3 , #datetimepicker4,#datetimepicker5, #datetimepicker6').datetimepicker({
            locale: 'ru',
            format: 'L',
            vertical:'top',
            startDate: '-3d'
        });
    }); 
    
    $('.burger, .overlay').click(function(){
        $('.burger').toggleClass('clicked');
        $('.overlay').toggleClass('show');
        $('nav').toggleClass('show');
        $('body').toggleClass('overflow');
    });
});

document.addEventListener("DOMContentLoaded", _ => {
    (function () {
        let y = window.scrollY;
        if (y > 10) {
            document.querySelector(".header").classList.add("sm");
        } else {
            document.querySelector(".header").classList.remove("sm");
        }
    })();
    
});

$(document).scroll(function() {
    // Header add remove class sm in scroll
    (function () {
        let y = window.scrollY;
        if (y > 10) { document.querySelector(".header").classList.add("sm");  }
        else { document.querySelector(".header").classList.remove("sm"); }
    })();
});
  
